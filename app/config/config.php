<?php

/**
 * Configuration
 *
 */

/**
 * Configuration for: Error reporting
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);

/**
 * Configuration for: URL
 */

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);

/**
 * Configuration for: Database
 */
define('DB_TYPE', 'mysql');
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'realpush_db');
define('DB_USER', 'realpush_db');
define('DB_PASS', 'kW5W3VBGTFJc');
define('DB_CHARSET', 'utf8');

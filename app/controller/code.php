<?php

/**
 * Class Code
 *
 */
class Code extends Controller
{

    /**
     * Create table if not exists 
     */
    public function create_table()
    {
        // sql to create table
        $sql = "CREATE TABLE IF NOT EXISTS Sample_code (
            id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
            code_ VARCHAR(7) NOT NULL,
            status BOOL DEFAULT 0
        )";
        $query = $this->db->exec($sql);
        return true;
    }

    /**
     *  Generate a random code of length 6 or 7 and store in the table
     */
    public function generate_and_store(){
        $code = $this->model->generate();
        $this->model->store(['code' => $code]);
    }

    /**
     *  Populate the Sample_Code table with near a million random data
     */
    public function populate(){
        // Number of codes to generate, the figure is arbitrary
        $number_of_codes = 785678;
        $chunk_size = 1000; 

        $this->model->seed($number_of_codes, $chunk_size);
    }

    /**
     *  Get the values as csv as well as html and change status
     *
     *  @param int $quantity is the number of records requested
     *  @param string $file is the name of the csv file
     *  @param int $column_number is the number of columns of the csv file
     *  @return view with the link to the csv file
     */
    public function generate_code(){
        $start_time = microtime(true);
    
        // Validating inputs
        $_SESSION['OLD'] = $_POST;
        if(empty($_POST['quantity']) || empty($_POST['file']) || empty($_POST['column_number'])){
            $_SESSION['Error'] = "Please provide all required data";
            header('Location: ' . URL);
            exit();
        }

        $quantity = (int) $_POST['quantity'];
        $file = $_POST['file'];
        $column_number = $_POST['column_number'];
        $file_ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = pathinfo($file, PATHINFO_FILENAME);

        // Validating file name
        if(empty($file_ext) || empty($file_name) || strtolower($file_ext) != 'csv'){
            $_SESSION['Error'] = "Please provide a valid file name (e.g. filename.csv)";
            header('Location: ' . URL);
            exit();
        }

        // Get codes from database
        $codes = $this->model->retrieve($quantity);

        // Store in a file
        $file_path = APP . 'storage/' . $file;
        $fp = fopen($file_path, 'w');

        $row_number = ceil($quantity/$column_number);
        $message = "Your Code is ";
        for($i = 0; $i < $row_number; $i++){
            for($j = 0; $j < $column_number; $j++){
                $index = $i * $column_number + $j;
                if($index >= $quantity) {
                    break;
                }
                $row[] = $message . '"' . $codes[$index] . '"';
            }
            $table[] = $row;
            fputcsv($fp, $row);
            unset($row);
        }

        fclose($fp);

        $end_time = microtime(true);

        $time_taken = (int) (($end_time - $start_time) * 1000);
        $_SESSION['file_name'] = $file;
        $_SESSION['Time_taken'] = $time_taken;
        $_SESSION['Table'] = $table;
        header('Location: ' . URL);
        exit();
    }

    public function download($file){
        $file_name = APP . 'storage/' . $file;
        header("Content-Length: " . filesize($file_name));
        header('Content-Type: text/csv; charset = utf-8');
        header("Content-Disposition: attachment; filename=$file");

        readfile($file_name);
    }
}

<?php

/**
 * Class Home
 *
 */
class Home extends Controller
{
    /**
     * The landing page of the web site
     */
    public function index()
    {
        require APP . 'view/partials/header.php';
        require APP . 'view/home/index.php';
        if(isset($_SESSION['Table'])){
            require APP . 'view/code/list.php';
        }
        require APP . 'view/partials/footer.php';
    }

    public function browse(){
        require APP . 'view/partials/header.php';
        require APP . 'view/home/codes.php';
        require APP . 'view/partials/footer.php';
    }

}

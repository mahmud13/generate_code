<?php

class Application
{
    /** @var The controller */
    private $controller = null;

    /** @var The method of the controller */
    private $method = null;

    /** @var The parameters of the method */
    private $params = array();

    /**
     * "Start" the application:
     */
    public function __construct()
    {
        // Analyze the URL determine the Controller, method and parameters
        $this->splitUrl();

        // check for controller
        if (!$this->controller) {
            // If no controller given, then load home page
            require APP . 'controller/home.php';
            $page = new Home();
            $page->index();

        } elseif (file_exists(APP . 'controller/' . $this->controller . '.php')) {

            // if controller exists, then load this file 
            require APP . 'controller/' . $this->controller . '.php';

            // example: if controller is "code", then the following line 
            // will be interpreted as: $this->controller = new code();
            $this->controller = new $this->controller();

            // Does such a method exist in the controller?
            if (method_exists($this->controller, $this->method)) {
                // If yes, are there parameters for that method?
                if (!empty($this->params)) {
                    // Yes, so call the method and pass the parameters to that
                    call_user_func_array(array($this->controller, $this->method), $this->params);
                } else {
                    // No parameters. So call the method without parameters
                    $this->controller->{$this->method}();
                }

            } else {
                // Method doesn't exist. But, is it empty, or a wrong method name?
                if (strlen($this->method) == 0) {
                    // If the method empty, call index() method
                    $this->controller->index();
                }
                else {
                    // A non existant method is called, show error
                    header('HTTP/1.0 404 Not Found');
                }
            }
        } else {
            // The controller doesn't exist
            header('HTTP/1.0 404 Not Found');
        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        ;
        if (isset($_SERVER['REQUEST_URI'])) {

            // split URL
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            $this->controller = isset($url[0]) ? $url[0] : null;
            $this->method = isset($url[1]) ? $url[1] : null;

            // Remove controller and method from the split URL
            unset($url[0], $url[1]);

            // Rebase array keys and store the  arguments
            $this->params = array_values($url);

        }
    }
}

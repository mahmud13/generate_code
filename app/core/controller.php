<?php

class Controller
{
    /**
     * @var Database Connection
     */
    public $db = null;

    /**
     * @var Model
     */
    public $model = null;

    /**
     *  Establish a database connection  and load the model.
     */
    function __construct()
    {
        $this->connectToDB();
        $this->loadModel();
    }

    /**
     * Open the database connection with the credentials from application/config/config.php
     */
    private function connectToDB()
    {
        // Set fetch mode to Objects
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

        // Connect to database using PDO
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
    }

    /**
     * Loads the "model".
     * @return object model
     */
    public function loadModel()
    {
        require APP . 'model/model.php';
        $this->model = new Model($this->db);
    }
}

<?php

class Model
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     * Store a code in the database
     * @param array $data holds values for 'code' and 'status'
     * @return bool true upon successful insertion
     */
    public function store($data)
    {
        if(!isset($data['code'])){
            return false;
        }
        
        // Get the code
        $code = $data['code'];

        // Get the status, if not found, set default value of zero
        $status = isset($data['status']) ? $data['status'] : 0;

        $sql = 'INSERT INTO Sample_code (code_, status) VALUES (:code, :status)';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':code', $code);
            $stmt->bindParam(':status', $status);
            $query->execute();
        }catch (PDOException $e) {
            exit('Error while storing data: ' . $e->getMessage);
        }

        return true;
    }
    /**
     *  Generate a random code of length 6 or 7
     *  @return string The generated Code
     */
    public function generate(){
        // An Array of capital letters
        $alphabet = range('A', 'Z');
        shuffle($alphabet);

        // Length of the generated Code is either 6 or 7
        $len_of_code = rand(6, 7);

        // The array is sliced from the initial position to the length of the code
        return implode("", array_slice($alphabet, 0, $len_of_code));
    }

    /**
     *  Seed the the table Sample_code with random codes 
     *
     *  @param int $number_of_codes is the number of codes to be generated for seeding
     *  @param int $chunk_size is the number of codes to be inserted each time a query runs
     *
     */

    public function seed($number_of_codes, $chunk_size = null){
        // Number of codes can be inserted as a chunk on a server having small
        // memory to avoid excessive memory load on the server with
        // an expense of a little amount of time

        // if the chunk size is null, the default value, codes won't be chunked
        // All codes will be generated and sent to database at a time
        // and the chunk size is equal to the number of codes.
        if (empty($chunk_size)) {
            $chunk_size = $number_of_codes;
        }

        // Example: if number of codes is 20 and chunk size is 6, number of equal chunks is 20/6 = 3 
        // and the size of the last chunk is 20%6 = 2.
        $number_of_chunks = floor($number_of_codes/$chunk_size);
        $size_of_last_chunk = $number_of_codes % $chunk_size;

        // First, generate and insert chunks having equal number of codes
        for($i = 0; $i < $number_of_chunks; $i++) {
            for($j = 0; $j < $chunk_size; $j++) {
                // We use prepared statements for security
                $rawSQL[] = "(?, 0)";
                $code[] = $this->generate();
            }
            $sql = 'INSERT INTO Sample_code (code_, status) VALUES ' . implode(',', $rawSQL);
            $stmt = $this->db->prepare($sql);
            $stmt->execute($code);

            // Now unset the variables, so no much pressure on server's memory
            unset($rawSQL);
            unset($code);
        }

        // Now, insert the last chunk if any
        for($j = 0; $j < $size_of_last_chunk; $j++) {
            $rawSQL[] = "(?, 0)";
            $code[] = $this->generate();
        }
        $sql = 'INSERT INTO Sample_code (code_, status) VALUES ' . implode(',', $rawSQL);
        $stmt = $this->db->prepare($sql);
        $stmt->execute($code);
    }

    /**
     *  Get the codes from database 
     *
     *  @param int $quantity is the number of records requested
     *  @return array of the codes
     */
    public function retrieve($quantity){
        try {
            // Select first $quantity number of codes status of which is zero
            $sql1 = 'SELECT code_ from Sample_code WHERE status = 0 LIMIT :quantity;';
            // Change the status of first $quantity number of codes status of which is zero
            // i.e. the codes selected by previous query
            $sql2 = 'UPDATE Sample_code SET status = 1 WHERE status = 0 LIMIT :quantity;';

            // Create a prepared statement
            $stmt = $this->db->prepare($sql1 . $sql2);

            // Bind parameters. PDO::PARAM_INT tells pdo to consider $quantity as an integer
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);

            // Execute the statement
            $stmt->execute();

            // Fetch the column code_ as  array and return it
            return $stmt->fetchAll(PDO::FETCH_COLUMN);
        }catch (PDOException $e) {
            exit('Error while fetching data: ' . $e->getMessage);
        }
    }

}

<div class="container">
    <?php 
        if(isset($_SESSION['Table'])){
            $table = $_SESSION['Table'];
            unset($_SESSION['Table']);
        }
        if(isset($_SESSION['Time_taken'])){
            $time = $_SESSION['Time_taken'];
            unset($_SESSION['Time_taken']);
        }
        if(isset($_SESSION['file_name'])){
            $file_name = $_SESSION['file_name'];
            unset($_SESSION['file_name']);
        }
    ?>
    <span style="font-size:2em; font-weight:bold">Time taken: <?php echo isset($time) ? $time : "" ; ?> ms</span>
    <?php if (!empty($file_name)): ?>
    <a target="_blank" href="/code/download/<?php echo $file_name ?>"><button style="margin-left:30px">Download CSV file</button></a>
    <?php endif ; ?>

    <table style="border:1px solid black; border-collapse: collapse">
        <?php foreach($table as $rows): ?>
        <tr style="border:1px solid black">
            <?php foreach($rows as $code): ?>
            <td style="border:1px solid black"> <?php echo $code; ?> </td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

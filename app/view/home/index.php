<div class="container">
    <form action="/code/generate_code" method="POST">
        <div style="font-weight:bold; font-size:1.1em; margin-bottom:10px;">Generate Code:</div>
        <div class="form-frame">
            <div class="error">
                <?php 
                    if(!empty($_SESSION['Error'])){ 
                        echo $_SESSION['Error'];
                        unset($_SESSION['Error']);
                    }
                  ?>
            </div>
                <?php
                    if(!empty($_SESSION['OLD'])){
                        $old = $_SESSION['OLD'];
                        unset($_SESSION['OLD']);
                    }
                ?>
            <div class="field">
                <label for="quantity">Quantity:</label>
                <input type="number" name="quantity" value="<?php echo isset($old['quantity']) ? $old['quantity'] : "" ; ?>" >
            </div>
            <div class="field">
                <label for="file">File Name:</label>
                <input type="text" name="file" value="<?php echo isset($old['file']) ? $old['file'] : "" ; ?>" >
            </div>
            <div class="field">
                <label for="column_number">Column Number:</label>
                <input type="number" name="column_number" value="<?php echo isset($old['column_number']) ? $old['column_number'] : "" ; ?>" >
            </div>
            <div class="submit-button">
                <button type="submit">Submit</button>
            </div>
        </div>
    </form>
</div>

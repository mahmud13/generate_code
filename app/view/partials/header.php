<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>API Code Generation</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-43bbf7e6e7b177c23ca12ba5d5fc9b26944fdb8f2bb890a6579aa7827b205e02.css" integrity="sha256-Q7v35uexd8I8oSul1fybJpRP248ruJCmV5qngnsgXgI=" media="all" rel="stylesheet">

    <!-- CSS -->
    <link href="<?php echo URL; ?>css/style.css" rel="stylesheet">
</head>
<body>
    <!-- logo -->
    <div class="logo">
        API Code generation
    </div>

    <!-- navigation -->
    <div class="navigation">
        <a href="<?php echo URL; ?>">Home</a>
    </div>

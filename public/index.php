<?php

/**
 * @package framework
 * @author Mahmud
 */
session_start();
// Root directory  
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
// Application directory
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

// Another Comment
// load configurations 
require APP . 'config/config.php';

// load application class
require APP . 'core/application.php';
require APP . 'core/controller.php';

// start the application
$app = new Application();
